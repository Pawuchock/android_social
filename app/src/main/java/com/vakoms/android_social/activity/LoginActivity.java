package com.vakoms.android_social.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import com.vakoms.android_social.R;
import com.vakoms.android_social.fragments.Login.SignUpLoginFragment;


/**
 * Created by tarasviter on 8/3/15.
 */
public class LoginActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragment(new SignUpLoginFragment(), R.id.container, false, SignUpLoginFragment.class.getSimpleName());

    }


    public void loadFragment(Fragment fragment, int id, boolean addToBackStack, String name) {
        try {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(id, fragment, name);

            if (addToBackStack) {
                transaction.addToBackStack(name);
            }
            transaction.commitAllowingStateLoss();
        } catch (Exception e) {
            //Logger.e(Const.TAG, "Failed to load Fragment : " + e.getMessage());
        }
        supportInvalidateOptionsMenu();
    }

}
