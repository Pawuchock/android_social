package com.vakoms.android_social.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by tarasviter on 8/3/15.
 */
public class BaseActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStart() {
        super.onStart();
     }

    @Override
    protected void onStop() {
        super.onStop();
    }
}

