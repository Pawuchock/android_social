package com.vakoms.android_social.fragments.Login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.vakoms.android_social.R;
import com.vakoms.android_social.fragments.BaseFragment;

/**
 * Created by tarasviter on 8/3/15.
 */
public class SignUpLoginFragment extends BaseFragment {
    Button signloginbtn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.signuplogin_layout, container, false);
        setupView(mView);
        return mView;
    }
    private void setupView(View view){
        signloginbtn = (Button) view.findViewById(R.id.sighuploginbtn);
        signloginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.loadFragment(new PhoneNumberFragment(),R.id.container, false, PhoneNumberFragment.class.getSimpleName());
            }
        });
    }

}

