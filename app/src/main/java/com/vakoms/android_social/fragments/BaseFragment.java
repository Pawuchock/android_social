package com.vakoms.android_social.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.vakoms.android_social.activity.LoginActivity;

/**
 * Created by tarasviter on 8/3/15.
 */
public class BaseFragment extends Fragment {
    public LoginActivity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (LoginActivity) activity;
    }

}
