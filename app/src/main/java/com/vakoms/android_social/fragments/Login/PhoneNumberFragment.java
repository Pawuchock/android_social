package com.vakoms.android_social.fragments.Login;

import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.vakoms.android_social.R;
import com.vakoms.android_social.fragments.BaseFragment;


public class PhoneNumberFragment extends BaseFragment {
    EditText numberfield;
    Button sendsmsbtn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.phonenumber_layout, container, false);
        setupView(mView);
        return mView;
    }
    private void setupView(View view){
    numberfield = (EditText) view.findViewById(R.id.numberfield);
        numberfield.setRawInputType(InputType.TYPE_CLASS_NUMBER);
    sendsmsbtn = (Button) view.findViewById(R.id.sendsmsbtn);
        sendsmsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.loadFragment(new VerificationFragment(),R.id.container, false, VerificationFragment.class.getSimpleName());
            }
        });


    }

}


