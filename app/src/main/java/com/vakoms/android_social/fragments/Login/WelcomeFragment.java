package com.vakoms.android_social.fragments.Login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.vakoms.android_social.R;
import com.vakoms.android_social.fragments.BaseFragment;

/**
 * Created by tarasviter on 8/3/15.
 */
public class WelcomeFragment extends BaseFragment {
    RelativeLayout touchzone;
    ImageView welcomeimg;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.welcome_fragment, container, false);
        setupView(mView);
        return mView;
    }

    public void setupView(View view) {
        touchzone = (RelativeLayout) view.findViewById(R.id.welcomeLayout);
        touchzone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  mActivity.loadFragment(new SignUpLoginFragment(),R.id.container, false, SignUpLoginFragment.class.getSimpleName());
            }
        });
    }
}
