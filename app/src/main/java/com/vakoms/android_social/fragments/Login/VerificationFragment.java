package com.vakoms.android_social.fragments.Login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.vakoms.android_social.R;
import com.vakoms.android_social.fragments.BaseFragment;

/**
 * Created by tarasviter on 8/3/15.
 */
public class VerificationFragment extends BaseFragment {
    Button submitbtn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.verification_layout, container, false);
        setupView(mView);
        return mView;
    }
    private void setupView(View view){
        submitbtn = (Button) view.findViewById(R.id.submitbtn);
        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.loadFragment(new WelcomeFragment(),R.id.container, false, WelcomeFragment.class.getSimpleName());
            }
        });
    }
}
